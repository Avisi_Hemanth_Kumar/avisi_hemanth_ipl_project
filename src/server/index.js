// Importing internal and external node modules.

import csvToJson from "convert-csv-to-json";
import jsonfile from "jsonfile";
import path from "path";
import {
    matchesPlayedPerYear,
    matchesOwnPerTeamPerYear,
    extraRuns2k16,
    economicalBowlers2k15,
    wonTossMath,
    highestPlayerOfMatch,
    dismissalByPlayer,
    bowlerEconomySo,
} from "./ipl.js";

// Using path module for converting Relative path to absolute path.

const file1 = path.resolve("src/public/output/matchesPerYear.json");
const file2 = path.resolve("src/public/output/matchesOwnPerTeam.json");
const file3 = path.resolve("src/public/output/extraRuns2k16.json");
const file4 = path.resolve("src/public/output/economicalBowlers2k15.json");
const file5 = path.resolve("src/public/output/wonTossMatch.json");
const file6 = path.resolve("src/public/output/highestPlayerOfMatch.json");
const file7 = path.resolve("src/public/output/dismissalByPlayer.json");
const file8 = path.resolve("src/public/output/bowlerEconomySo.json");

// Converting the csv files to JSON Array format.

const matchesData = csvToJson
    .fieldDelimiter(",")
    .formatValueByType()
    .getJsonFromCsv(path.resolve("src/data/matches.csv"));
const deliveriesData = csvToJson
    .fieldDelimiter(",")
    .formatValueByType()
    .getJsonFromCsv(path.resolve("src/data/deliveries.csv"));

/**
 * Sends result objects from modules to output json files.
 * @param {Object} object - The Resultant object which was returned from module used.
 * @param {file} file - Output file to which Objects to be dumped.
 * @param {Object} jsonfile - Object from external module which was imported.
 */

function sendToJson(object, file, jsonfile) {
    jsonfile.writeFile(
        file,
        object, {
            spaces: 1,
        },
        function (err) {
            if (err) console.error(err);
        }
    );
}

// Number of matches played per year for all the years in IPL.

const matchesPlayedPerYearStats = matchesPlayedPerYear(matchesData);
sendToJson(matchesPlayedPerYearStats, file1, jsonfile);

// Number of matches won per team per year in IPL.

const matchesOwnPerTeamPerYearStats = matchesOwnPerTeamPerYear(matchesData);
sendToJson(matchesOwnPerTeamPerYearStats, file2, jsonfile);

// Extra runs conceded per team in the year 2016

const extraRuns2k16Stats = extraRuns2k16(deliveriesData, matchesData);
sendToJson(extraRuns2k16Stats, file3, jsonfile);

// Top 10 economical bowlers in the year 2015

const economicalBowlers2k15Stats = economicalBowlers2k15(
    deliveriesData,
    matchesData
);
sendToJson(economicalBowlers2k15Stats, file4, jsonfile);

// Number of times each team won the toss and also won the match.

const wonTossMathStats = wonTossMath(matchesData);
sendToJson(wonTossMathStats, file5, jsonfile);

// Player who has won the highest number of Player of the Match awards for each season.

const highestPlayerOfMatchStats = highestPlayerOfMatch(matchesData);
sendToJson(highestPlayerOfMatchStats, file6, jsonfile);

// Highest number of times one player has been dismissed by another player.

const dismissalByPlayerStats = dismissalByPlayer(deliveriesData);
sendToJson(dismissalByPlayerStats, file7, jsonfile);

// Bowler with the best economy in super overs

const bowlerEconomySoStats = bowlerEconomySo(deliveriesData);
sendToJson(bowlerEconomySoStats, file8, jsonfile);