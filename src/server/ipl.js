export {
    matchesPlayedPerYear,
    matchesOwnPerTeamPerYear,
    extraRuns2k16,
    economicalBowlers2k15,
    wonTossMath,
    highestPlayerOfMatch,
    dismissalByPlayer,
    bowlerEconomySo,
};

/**
 * Number of matches played per year for all the years in IPL.
 * @param {Array} matches - Array of objects containing matches data.
 * @returns {Object} - Number of matches played per year for all the years in IPL.
 */

function matchesPlayedPerYear(matches) {
    const seasons = matches.map((match) => match.season);
    const uniqueSeason = [...new Set(seasons)];
    const matchesPlayed = uniqueSeason.map((season) => [
        season,
        seasons.filter((year) => year === season).length,
    ]);

    return Object.fromEntries(matchesPlayed);
}

/**
 * Number of matches won per team per year in IPL.
 * @param {Array} matches - Array of objects containing matches data.
 * @returns {Object} - Number of matches won per team per year in IPL.
 */

function matchesOwnPerTeamPerYear(matches) {
    const seasons = matches.map((match) => match.season);
    const uniqueSeason = [...new Set(seasons)];

    const matchesOwnYear = uniqueSeason.map((season) => {
        const seasonMatches = matches.filter((match) => season === match.season);
        const winners = seasonMatches.map((match) => match.winner);
        const uniqueWinner = [...new Set(winners)];
        const matchesOwn = uniqueWinner.map((winner) => [
            winner,
            winners.filter((won) => won === winner).length,
        ]);

        return [season, Object.fromEntries(matchesOwn)];
    });

    return Object.fromEntries(matchesOwnYear);
}

/**
 * Extra runs conceded per team in the year 2016
 * @param {Array} deliveries - Array of objects containing deliveries data.
 * @param {Array} matches - Array of objects containing matches data.
 * @returns {Object} - Extra runs conceded per team in the year 2016
 */

function extraRuns2k16(deliveries, matches) {
    const matches2K16 = matches.filter((match) => match.season === 2016);
    const matchId2K16 = matches2K16.map((match) => match.id);
    const deliveries2k16 = deliveries.filter(
        (delivery) => matchId2K16.includes(delivery.match_id) === true
    );
    const extraRuns2k16 = deliveries2k16.map((delivery) => delivery.extra_runs);
    const extraRuns = extraRuns2k16.reduce(
        (runs, currentRun) => runs + currentRun
    );
    return extraRuns;
}

/**
 * Top 10 economical bowlers in the year 2015
 * @param {Array} deliveries - Array of objects containing deliveries data.
 * @param {Array} matches - Array of objects containing matches data.
 * @returns {Object} - Top 10 economical bowlers in the year 2015
 */

function economicalBowlers2k15(deliveries, matches) {
    const matches2K15 = matches.filter((match) => match.season === 2015);
    const matchId2K15 = matches2K15.map((match) => match.id);
    const deliveries2k15 = deliveries.filter(
        (delivery) => matchId2K15.includes(delivery.match_id) === true
    );
    const bowlers = deliveries2k15.map((delivery) => delivery.bowler);
    const uniqueBowlers = [...new Set(bowlers)];
    const bowlersEconomy = uniqueBowlers.map((bowler) => {
        const bowlerDeliveries = deliveries2k15.filter(
            (delivery) => delivery.bowler === bowler
        );
        const bowlerEconomy = bowlerDeliveries.reduce(
            (result, delivery) => {
                result.totalRuns += delivery.total_runs;
                result.totalBalls +=
                    delivery.wide_runs === 0 && delivery.noball_runs === 0 ? 1 : 0;
                return result;
            }, {
                totalRuns: 0,
                totalBalls: 0,
            }
        );
        return [bowler, bowlerEconomy];
    });

    const economicalBowlers = bowlersEconomy.map((bowler) => {
        const over = bowler[1].totalBalls / 6;
        const runs = bowler[1].totalRuns;
        const economy = runs !== 0 && over !== 0 ? (runs / over).toFixed(2) : 0;
        bowler[1] = economy;
        return bowler;
    });

    economicalBowlers.sort((prev, curr) => prev[1] - curr[1]);
    const bowlersEconomyTop10 = economicalBowlers.slice(0, 10);
    return Object.fromEntries(bowlersEconomyTop10);
}

/**
 * Number of times each team won the toss and also won the match.
 * @param {Array} matches - Array of objects containing matches data.
 * @returns {Object} - Number of times each team won the toss and also won the match.
 */

function wonTossMath(matches) {
    const wonTossMath = matches.filter(
        (match) => match.toss_winner === match.winner
    );
    return wonTossMath.length;
}

/**
 * Player who has won the highest number of Player of the Match awards for each season.
 * @param {Array} matches - Array of objects containing matches data.
 * @returns {Object} - Player who has won the highest number of Player of the Match awards for each season.
 */

function highestPlayerOfMatch(matches) {
    const matchesByYear = matches.reduce((acc, co) => {
        const key = co["season"];
        if (acc[key] === undefined) {
            acc[key] = [];
        }
        acc[key].push(co.player_of_match);
        return acc;
    }, {});

    const groupByYear = Object.entries(matchesByYear);

    const result = groupByYear.map((year) => {
        const players = year[1];
        const playersCount = players.reduce((players, player) => {
            if (player in players) {
                players[player] += 1;
            } else {
                players[player] = 1;
            }
            return players;
        }, {});

        const playerArray = Object.entries(playersCount);
        const sorted = playerArray.sort((a, b) => b[1] - a[1]);
        return [year[0], sorted[0][0]];
    });

    return Object.fromEntries(result);
}

/**
 * Highest number of times one player has been dismissed by another player.
 * @param {Array} deliveries - Array of objects containing deliveries data.
 * @returns {Object} - Highest number of times one player has been dismissed by another player.
 */

function dismissalByPlayer(deliveries) {
    const dismissal = deliveries.filter(
        (delivery) => delivery.player_dismissed !== "" && delivery.fielder !== ""
    );

    return dismissal.length;
}

/**
 * Bowler with the best economy in super overs
 * @param {Array} deliveries - Array of objects containing deliveries data.
 * @returns {Object} - Bowler with the best economy in super overs
 */

function bowlerEconomySo(deliveries) {
    const superDeliveries = deliveries.filter(
        (delivery) => delivery.is_super_over !== 0
    );
    const bowlers = superDeliveries.map((delivery) => delivery.bowler);
    const uniqueBowlers = [...new Set(bowlers)];
    const bowlersEconomy = uniqueBowlers.map((bowler) => {
        const bowlerDeliveries = superDeliveries.filter(
            (delivery) => delivery.bowler === bowler
        );
        const bowlerEconomy = bowlerDeliveries.reduce(
            (result, delivery) => {
                result.totalRuns += delivery.total_runs;
                result.totalBalls +=
                    delivery.wide_runs === 0 && delivery.noball_runs === 0 ? 1 : 0;
                return result;
            }, {
                totalRuns: 0,
                totalBalls: 0,
            }
        );
        return [bowler, bowlerEconomy];
    });

    const economicalBowlers = bowlersEconomy.map((bowler) => {
        const over = bowler[1].totalBalls / 6;
        const runs = bowler[1].totalRuns;
        const economy = runs !== 0 && over !== 0 ? (runs / over).toFixed(2) : 0;
        bowler[1] = economy;
        return bowler;
    });

    economicalBowlers.sort((prev, curr) => prev[1] - curr[1]);
    const bestBowler = economicalBowlers.slice(0, 1);
    return Object.fromEntries(bestBowler);
}