#Getting Started

## Clone the repository:
git clone https://gitlab.com/Avisi_Hemanth_Kumar/avisi_hemanth_ipl_project

## CD to project cloned directory
cd dirName

## Install all the dependencies
npm i

## npm start
Runs index.js and starts http-server

## Output in JSON format
The output of the project is saved in a folder in JSON format.
Output folder path:
/src/public/output

## Deployment Link
https://pedantic-liskov-2cb955.netlify.app/
